package main

import (
	"fmt"
	"io"
	"math/rand"
	"net/http"
	nurl "net/url"
	"os"
	"os/exec"
	"path"
	fp "path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/Jeffail/gabs"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"gopkg.in/ini.v1"
)

func erred(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// Thanks to http://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-golang
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func randString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

const twitRegex string = `^http(s?):\/\/pbs\.twimg\.com\/media\/[^\./]+\.(jpg|png)((\:[a-z]+)?)$`
const tistRegex string = `^http(s?):\/\/[a-z0-9]+\.uf\.tistory\.com\/(image|original)\/[A-Z0-9]+$`
const fileRegex string = `filename=[^\./]+\.[^\./]+`
const youtRegex string = `^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$`

func downTweet(imgURL string) {
	// Thanks to http://stackoverflow.com/questions/22417283/save-an-image-from-url-to-file
	resp, errGet := http.Get(imgURL + ":orig")
	urlRequestError(errGet)

	match, errCompile := regexp.Compile(twitRegex)
	regexCompileError(errCompile)

	twitterURL := match.FindString(imgURL)
	twitterFilename := strings.Split(twitterURL, "/")

	defer func() {
		errClose := resp.Body.Close()
		closeError(errClose)
	}()

	var slice int
	for i, element := range twitterFilename {
		if element != "" {
			slice = i
		}
	}

	// Assign random filename and into twitter filename
	tempName := randString(8)
	filePath := "./" + *userFlag + "/" + tempName
	file, errCreate := os.Create(filePath)
	createError(errCreate)

	_, errCopy := io.Copy(file, resp.Body)
	copyError(errCopy)
	defer func() {
		errClose := file.Close()
		closeError(errClose)
	}()

	errRename := os.Rename(filePath, "./"+*userFlag+"/"+twitterFilename[slice])
	renameError(errRename)
}

func downURL(url string) {
	// TODO: handle videos

	// If redirects, get the new url as main string
	request, errRequest := http.NewRequest("GET", url, nil)
	urlRequestError(errRequest)

	request.Header.Add("Accept-Encoding", "identity")
	client := &http.Client{Timeout: 300}
	resp, errDo := client.Do(request)
	if errDo != nil {
		logrus.Error("Error doing request!")
	}
	erred(errDo)

	newURL := resp.Request.URL.String()

	if newURL != "" {
		defer func() {
			errClose := resp.Body.Close()
			closeError(errClose)
		}()
		url = newURL
	}

	// If Tistory url, replace /image/ with /original/
	match, errCompile := regexp.Compile(tistRegex)
	regexCompileError(errCompile)
	image := regexp.MustCompile("image")

	tistoryURL := match.FindString(url)

	if tistoryURL != "" {
		tistoryOrigURL := image.ReplaceAllLiteralString(url, "original")
		url = tistoryOrigURL
	}

	resp, errGet := http.Get(url)
	urlRequestError(errGet)

	// Check content-type if image/video and -disposition for filename
	contentType := resp.Header.Get("Content-Type")
	header := resp.Header.Get("Content-Disposition")

	name, errCompile := regexp.Compile(fileRegex)
	regexCompileError(errCompile)

	nameSlice := name.FindStringSubmatch(header)
	var nameString string
	for i, element := range nameSlice {
		if element != "" {
			nameString = nameSlice[i]
		}
	}

	realFilename := strings.Split(nameString, "=")

	defer func() {
		errClose := resp.Body.Close()
		closeError(errClose)
	}()

	var slice int
	for i, element := range realFilename {
		if element != "" {
			slice = i
		}
	}

	// Assign random filename and into real filename
	assignName(contentType, resp, realFilename, slice, url)
}

func assignName(ctype string, resp *http.Response, realFilename []string, slice int, url string) {
	if strings.Contains(ctype, "image") || strings.Contains(ctype, "video") {
		tempname := randString(8)
		filepath := "./" + *userFlag + "/" + tempname
		file, errCreate := os.Create(filepath)
		createError(errCreate)

		_, errCopy := io.Copy(file, resp.Body)
		copyError(errCopy)

		errClose := file.Close()
		closeError(errClose)

		if realFilename[slice] == "" {
			base := path.Base(url)
			// Use URL for filename
			pathname := "./" + *userFlag + "/" + strings.Split(base, "?")[0]
			i := 1
			if _, errStat := os.Stat(pathname); errStat == nil {
				newPath := pathname
				for {
					newPath = pathname[0:len(pathname)-len(fp.Ext(pathname))] +
						"-" + strconv.Itoa(i) + fp.Ext(pathname)
					if _, err := os.Stat(newPath); os.IsNotExist(err) {
						break
					}
					i = i + 1
				}
				newPath, _ = nurl.QueryUnescape(newPath)
				err := os.Rename(filepath, newPath)
				renameError(err)
			} else {
				pathname, _ = nurl.QueryUnescape(pathname)
				err := os.Rename(filepath, pathname)
				renameError(err)
			}
		} else {
			// Remove "s from "{filename}"
			pathname := "./" + *userFlag + "/" + strings.Trim(realFilename[slice], `"`)
			i := 1
			if _, errStat := os.Stat(pathname); errStat == nil {
				newPath := pathname
				for {
					newPath = pathname[0:len(pathname)-len(fp.Ext(pathname))] +
						"-" + strconv.Itoa(i) + fp.Ext(pathname)
					if _, err := os.Stat(newPath); os.IsNotExist(err) {
						break
					}
					i = i + 1
				}
				newPath, _ = nurl.QueryUnescape(newPath)
				err := os.Rename(filepath, newPath)
				renameError(err)
			} else {
				pathname, _ = nurl.QueryUnescape(pathname)
				err := os.Rename(filepath, pathname)
				renameError(err)
			}
		}
	}
}

func downYou(youURL string) {
	// Thanks to https://github.com/sn0w/Karen/blob/master/modules/plugins/music.go#L452
	ytJSON := exec.Command("youtube-dl", "-J", youURL)
	yjOut, errOut := ytJSON.Output()
	if errOut != nil {
		logrus.Error("Error reading yt-dl output!")
	}
	erred(errOut)

	json, errParse := gabs.ParseJSON(yjOut)
	if errParse != nil {
		logrus.Error("Error parsing JSON!")
	}
	erred(errParse)

	title := json.Path("title").Data().(string)
	ext := json.Path("ext").Data().(string)

	cmd := exec.Command(
		"youtube-dl",
		"--no-call-home",
		"--no-cache-dir",
		"-o", "-",
		youURL,
	)

	// Thanks to http://stackoverflow.com/questions/18986943/
	outfile, errCreate := os.Create("./" + *userFlag + "/" + title + "." + ext)
	if errCreate != nil {
		logrus.Error("Error creating file!")
	}
	erred(errCreate)
	defer func() {
		errClose := outfile.Close()
		closeError(errClose)
	}()
	cmd.Stdout = outfile

	errStart := cmd.Start()
	if errStart != nil {
		logrus.Error("Error starting command!")
	}
	erred(errStart)
	errWait := cmd.Wait()
	if errWait != nil {
		logrus.Error("Error waiting on command!")
	}
	erred(errWait)
}

var userFlag = flag.StringP("username", "u", "", "User to download from.")
var youFlag = flag.BoolP("youtube", "y", false, "Whether or not to download YouTube links.")

func main() {
	// TODO: Figure out why sometimes this panics

	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n canapart --username username --youtube true\n or canapart -u username -y false\n", os.Args[0])
	}

	cfg, err := ini.Load("config.ini")
	if err != nil {
		logrus.Error("Unable to read config file!")
		erred(err)
		cfg = ini.Empty()
	}

	if !cfg.Section("twitter").HasKey("consumer key") {
		_, err = cfg.Section("twitter").NewKey("consumer key", "your consumer key")
		erred(err)
		_, err = cfg.Section("twitter").NewKey("consumer secret", "your consumer secret")
		erred(err)
		_, err = cfg.Section("twitter").NewKey("access token", "your access token")
		erred(err)
		_, err = cfg.Section("twitter").NewKey("access token secret", "your access token secret")
		erred(err)
		err = cfg.SaveTo("config.ini")

		if err != nil {
			logrus.Error("Unable to write config file!")
			erred(err)
			return
		}
		logrus.Info("Wrote config file, please fill out and restart the program")
		return
	}

	flag.Parse()
	if *userFlag == "" {
		flag.Usage()
		flag.PrintDefaults()
		os.Exit(1)
	}

	errMkDir := os.MkdirAll("./"+*userFlag+"/", 0777)
	if errMkDir != nil {
		logrus.Error("Error creating directory!")
	}
	erred(errMkDir)

	config := oauth1.NewConfig(cfg.Section("twitter").Key("consumer key").String(),
		cfg.Section("twitter").Key("consumer secret").String())
	token := oauth1.NewToken(cfg.Section("twitter").Key("access token").String(),
		cfg.Section("twitter").Key("access token secret").String())
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	twitUser, _, err := client.Users.Show(&twitter.UserShowParams{
		ScreenName: *userFlag,
	})
	if err != nil {
		logrus.Error("Error showing Twitter user!")
	}
	erred(err)

	// Loop until maxID keeps repeating as the lastID given
	loop(client, twitUser)

	print("Downloading finished!\n")
}

func loop(client *twitter.Client, twitUser *twitter.User) {
	var userID = twitUser.ID
	var maxID int64
	var lastID int64
	var end = false

	for !end {
		tweets, _, err := client.Timelines.UserTimeline(&twitter.UserTimelineParams{
			UserID: userID,
			Count:  200,
			MaxID:  maxID,
		})
		if err != nil {
			logrus.Error("Error iterating over Twitter user timeline!")
		}
		erred(err)

		lastID = maxID

		print("Downloading available tweets... please wait!\n")
		for _, tweet := range tweets {
			if tweet.Entities != nil {
				for _, tweetURL := range tweet.Entities.Urls {
					ytMatch, err := regexp.Compile(youtRegex)
					regexCompileError(err)

					if ytMatch.FindString(tweetURL.ExpandedURL) != "" && *youFlag {
						downYou(tweetURL.ExpandedURL)
						logrus.Info("Downloading YouTube video: " + tweetURL.ExpandedURL)
					} else {
						downURL(tweetURL.ExpandedURL)
						logrus.Info("Downloading image: " + tweetURL.ExpandedURL)
					}
				}
			}
			tweetMedia(tweet)
			maxID = tweet.ID
		}
		if lastID == maxID {
			end = true
		}
	}
}

func tweetMedia(tweet twitter.Tweet) {
	if tweet.ExtendedEntities != nil {
		for _, tweetMedia := range tweet.ExtendedEntities.Media {
			if tweetMedia.VideoInfo.Variants == nil {
				downTweet(tweetMedia.MediaURLHttps)
				logrus.Info("Downloading Twitter video: " + tweetMedia.MediaURLHttps)
			} else if tweetMedia.VideoInfo.Variants != nil {
				// Thanks to https://github.com/Seklfreak/discord-image-downloader-go/blob/master/main.go#L715
				var lastVideoVariant twitter.VideoVariant
				for _, videoVariant := range tweetMedia.VideoInfo.Variants {
					if videoVariant.Bitrate >= lastVideoVariant.Bitrate {
						lastVideoVariant = videoVariant
					}
				}
				if lastVideoVariant.URL != "" {
					downURL(lastVideoVariant.URL)
					logrus.Info("Downloading image: " + lastVideoVariant.URL)
				}
			}
		}
	}
}

func urlRequestError(reqErr error) {
	if reqErr != nil {
		logrus.Error("Error requesting URL!")
		erred(reqErr)
	}
}

func regexCompileError(compileErr error) {
	if compileErr != nil {
		logrus.Error("Error compiling regex!")
		erred(compileErr)
	}
}

func closeError(closeErr error) {
	if closeErr != nil {
		logrus.Error("Error closing file or response!")
		erred(closeErr)
	}
}

func renameError(renameErr error) {
	if renameErr != nil {
		logrus.Error("Error renaming file!")
		erred(renameErr)
	}
}

func copyError(copyErr error) {
	if copyErr != nil {
		logrus.Error("Error copying file or response body!")
		erred(copyErr)
	}
}

func createError(createErr error) {
	if createErr != nil {
		logrus.Error("Error creating file!")
		erred(createErr)
	}
}
