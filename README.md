# canapart
[![Go Report Card](https://goreportcard.com/badge/github.com/tangerinetulip/canapart)](https://goreportcard.com/report/github.com/tangerinetulip/canapart) [![Build Status](https://travis-ci.org/tangerinetulip/canapart.svg?branch=master)](https://travis-ci.org/tangerinetulip/canapart)

My Twitter timeline downloader.
